export const trainRequest = station => {
  return {
    type: "SEARCH_REQUEST",
    station
  };
};
