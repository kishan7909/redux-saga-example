const initstate = {
  featching: false,
  trains: [],
  error: null
};

export const reducer = (state = initstate, action) => {
  console.log("Reducer Call", action);

  switch (action.type) {
    case "SEARCH_REQUEST":
      console.log("Search Request Call");
      return Object.assign({}, state, { featching: true, error: null });
    case "SEARCH_REQUEST_SUCCESS":
      return Object.assign({}, state, {
        featching: false,
        trains: action.train
      });
    case "SEARCH_REQUEST_FAIL":
      return Object.assign({}, state, {
        featching: false,
        trains: [],
        error: action.err.message
      });
    default:
      return state;
  }
};
