/* eslint-disable jsx-a11y/scope */
import React from "react";

const TrainList = props => {
  return (
    <React.Fragment>
      {props.error === null ? (
        <table className="table table-striped table-inverse table-responsive mt-5">
          <thead className="thead-inverse">
            <tr>
              <th>Name</th>
              <th>Number</th>
              <th>Actual arrival Time</th>
              <th>Arrive at</th>
              <th>Actual departure Time</th>
              <th>Departure at</th>
              <th>Delay arrival Time</th>
              <th>Delay departure Time</th>
            </tr>
          </thead>
          <tbody>
            {props.trains.map((trains, index) => {
              return (
                <tr key={index}>
                  <td scope="row">{trains.name}</td>
                  <td>{trains.number}</td>
                  <td>{trains.scharr}</td>
                  <td>{trains.actarr}</td>
                  <td>{trains.schdep}</td>
                  <td>{trains.actdep}</td>
                  <td style={{ color: "red" }}>{trains.delayarr}</td>
                  <td style={{ color: "red" }}>{trains.delaydep}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        <h2 className="text-center mt-4" style={{ color: "red" }}>
          {props.error}
        </h2>
      )}
    </React.Fragment>
  );
};

export default TrainList;
