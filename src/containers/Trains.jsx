import React, { Component } from "react";
import { connect } from "react-redux";
import { trainRequest } from "../store/actions/index";

class Trains extends Component {
  state = {
    station: ""
  };
  handleStation = e => {
    this.setState({ station: e.target.value });
    console.log(this.state.station);
  };
  render() {
    return (
      <React.Fragment>
        <div className="form-inline mt-5">
          <div className="form-group">
            <label htmlFor="station">Station Code</label>&nbsp;
            <input
              onChange={this.handleStation}
              type="text"
              name="station"
              id="station"
              className="form-control"
              placeholder="eg. ST"
              aria-describedby="helpId"
            />&nbsp;
          </div>
          <div className="form-group">
            {this.props.featching ? (
              <React.Fragment>
                <div className="spinner-border text-success " role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </React.Fragment>
            ) : (
              <button
                onClick={e => this.props.searchTrain(this.state.station)}
                type="button"
                className="btn btn-success"
              >
                Search
              </button>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    featching: state.featching,
    error: state.error
  };
};
const mapDispatchToProps = dispatch => {
  return {
    searchTrain: station => {
      console.log("Search Click");
      const action = trainRequest(station);
      dispatch(action);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Trains);
