import { call, takeLatest, put, delay } from "redux-saga/effects";
import axios from "axios";

export function* watcherSaga() {
  console.log("Saga watcher Call");
  yield takeLatest("SEARCH_REQUEST", workerSaga);
}

function* workerSaga(action) {
  console.log("saga worker call", action);
  yield delay(3000);
  try {
    const response = yield call(() => fetchtrain(action.station));
    const train = response.data.trains;
    yield put({ type: "SEARCH_REQUEST_SUCCESS", train });
  } catch (err) {
    yield put({ type: "SEARCH_REQUEST_FAIL", err });
  }
}

function fetchtrain(station) {
  console.log();
  return axios({
    method: "get",
    url:
      "http://api.railwayapi.com/v2/arrivals/station/" +
      station +
      "/hours/2/apikey/tbojrlyycl/"
  });
}
