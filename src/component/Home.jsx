import React, { Component } from "react";
import TrainList from "../containers/TrainList";
import Trains from "../containers/Trains";
import { connect } from "react-redux";
// import { trainRequest } from "../store/actions/index";

class Home extends Component {
  state = {
    station: ""
  };
  handleStation = e => {
    this.setState({ station: e.target.value });
    console.log(this.state.station);
  };
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <h1 className="mt-2">Trains Arrivals</h1>
          <Trains />
          <TrainList trains={this.props.trains} error={this.props.error} />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    featching: state.featching,
    trains: state.trains,
    error: state.error
  };
};
// const mapDispatchToProps = dispatch => {
//   return {
//     searchTrain: e => {
//       e.preventDefault();
//       console.log("Search Click");
//       const action = trainRequest(state.station);
//       dispatch(action);
//     }
//   };
// };

export default connect(mapStateToProps)(Home);
